#pragma once
#ifndef CONSOLE_H
#define CONSOLE_H

#include <Windows.h>
#include <stdio.h>
class Console
{
public:
	Console();
	~Console();
	void Clear();
	void SetCursorPos(COORD dwPos);
	void GetConsoleInfo();
	void Write(const char * input);
	void WritePointer(uintptr_t input);
	void WriteF(const char * fmt, ...);
	void Reset();
private:
	HANDLE hOut;
	COORD dwSize;
	COORD dwCursorPos;
	SMALL_RECT srWindow;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
};

#endif // !CONSOLE_H


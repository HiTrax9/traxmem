#include "D3DHook.h"



CD3DHook::CD3DHook()
{
}

CD3DHook::~CD3DHook()
{
}

void * CD3DHook::GetDxDeviceFuncByIndex(int iIndex)
{
	if (pDevice == nullptr)
	{
		IDirect3D9* pD3D = Direct3DCreate9(D3D_SDK_VERSION);
		if (pD3D)
		{
			D3DPRESENT_PARAMETERS d3dPP = { 0 };
			d3dPP.SwapEffect = D3DSWAPEFFECT_DISCARD;
			d3dPP.hDeviceWindow = GetForegroundWindow();
			d3dPP.Windowed = TRUE;

			IDirect3DDevice9* pDummy = nullptr;

			HRESULT hr = pD3D->CreateDevice(
				D3DADAPTER_DEFAULT,
				D3DDEVTYPE_HAL,
				d3dPP.hDeviceWindow,
				D3DCREATE_HARDWARE_VERTEXPROCESSING,
				&d3dPP, &pDummy);

			void** vTable = *(void***)(pDummy);

			return vTable[iIndex];
		}
	}
	else
	{
		void** vTable = *(void***)(pDevice);
		return vTable[iIndex];
	}
	return nullptr;
}

void * CD3DHook::GetFunctionThisPtr(void * pFunc)
{
	//this is where we'll save the pointer, create code cave
	void * pLoc = VirtualAlloc(nullptr, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	ZeroMemory(pLoc, 0x100);

	//this is where code will go
	uintptr_t pFuncLoc = (uintptr_t)pLoc + sizeof(uintptr_t);

	//build cave
	BYTE buff[] = { 0x89, 0x0D };
	TraxMem::WriteBytes(pFuncLoc, buff, 2);
	TraxMem::WriteValue<uintptr_t>(pFuncLoc + 2, (uintptr_t)(pLoc));  // mov [thisPtr], ecx

	BYTE buff1[] = { 0x6A, 0x18, 0xB8, 0x0A, 0x61, 0xF7, 0x62, 0xE9 };
	TraxMem::WriteBytes(pFuncLoc + 2 + sizeof(uintptr_t), buff1, 8);

	//calculate jmp back
	uintptr_t jmpBack = (uintptr_t)pFunc - (pFuncLoc + 2 + sizeof(uintptr_t) + 8) + 3;
	TraxMem::WriteValue<uintptr_t>(pFuncLoc + 2 + sizeof(uintptr_t) + 8, jmpBack);

	//hook
	TraxMem::Hook((uintptr_t)pFunc, pFuncLoc, 7);

	//wait til space is filled
	while (*(uintptr_t*)pLoc == 0);

	//read it
	uintptr_t thisPtr = *(uintptr_t*)pLoc;

	//clean up
	BYTE ogBytes[] = { 0x6A, 0x18, 					// push 18
		0xB8, 0x0A, 0x61, 0xF7, 0x62 };				// mov eax, d3d8.dll+7610A

	TraxMem::WriteBytes((uintptr_t)pFunc, ogBytes, 7); //unhook

	VirtualFree(pLoc, 0x100, MEM_RELEASE);

	return (void*)thisPtr;
}

bool CD3DHook::Hook(HOOKMETHOD hookMethod)
{
	switch (hookMethod)
	{
		case HOOKMETHOD::TRAMP:
		{
			
			break;
		}
	}
	return false;
}

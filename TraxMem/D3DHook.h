#pragma once
#ifndef CD3DHOOK_H
#define CD3DHOOK_H
#include <d3d9.h>
//#include <d3dx9.h>
#include "TraxMem/TraxIncludes.h"
#include "TraxMem/TraxMem.h"

#pragma comment(lib, "d3d9.lib")
//#pragma comment(lib, "d3dx9.lib")

class CD3DHook
{
public:
	enum HOOKMETHOD
	{
		TRAMP,
		VMT,
		CALLER
	};
public:
	CD3DHook();
	~CD3DHook();
	void * GetDxDeviceFuncByIndex(int iIndex);
	void * GetFunctionThisPtr(void * pFunc);
	bool Hook(HOOKMETHOD hookMethod);
private:
	IDirect3DDevice9 * pDevice = nullptr;
	std::string sError;
};

#endif // !CD3DHOOK_H
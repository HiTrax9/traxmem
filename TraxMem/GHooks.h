#pragma once
#ifndef GHOOKS_H
#define GHOOKS_H
#include "TraxMem/TraxIncludes.h"
#include <stdio.h>
#include <d3d9.h>

typedef BOOL (__stdcall * tGetAsyncKeyState)(DWORD vKey);
extern tGetAsyncKeyState oGetAsyncKeyState;
BOOL WINAPI hGetAsyncKeyState(DWORD vKey);

typedef HRESULT(__stdcall * _EndScene)(IDirect3DDevice9* pDevice);
extern _EndScene oEndScene;
HRESULT WINAPI hEndScene(IDirect3DDevice9* pDevice);

#endif // !GHOOKS_H

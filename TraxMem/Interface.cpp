#include "Interface.h"
#include <ctime>


CInterface::CInterface()
{
	pCon = &con;
	con.Write("Loaded!\n");
	//pEntitiesMPdll = (uintptr_t)GetModuleHandleA("entitiesmp.dll");
	//infAmmo.pAddress = pEntitiesMPdll + infAmmo.offset;
	////con.WriteF("InfAmmo.Address: %p\n", infAmmo.pAddress);


	////SPattern DecHealthPattern = SPattern("\xd9\x9e\x00\x00\x00\x00\xe8\x00\x00\x00\x00\x8b\x7d", "xx????x????xx");
	//SPattern DecHealthPattern = SPattern("d9 9e ? ? ? ? e8 ? ? ? ? 8b 7d");
	//uintptr_t pDecHealth = 0;

	//
	//if (!TraxMem::PatternScanMod(DecHealthPattern, "engine.dll", pDecHealth))
	//	con.Write("Pattern scan failed!\n");
	//
	//if (!TraxMem::PatternScanProcess(DecHealthPattern, pDecHealth))
	//	con.Write("Pattern scan failed!\n");

	//
	//pEngineDll = (uintptr_t)GetModuleHandleA("engine.dll");
	////godMode.pSource = pEngineDll + godMode.pSourceOffset;
	//godMode.pSource = pDecHealth;
	////con.WriteF("GodMode.Address: %p\n", godMode.pSource);
	Init();
}


CInterface::~CInterface()
{
}

bool CInterface::Update()
{
	if (GetAsyncKeyState(VK_END))
	{
		Unload();
		return false;
	}

	//if (GetAsyncKeyState(VK_NUMPAD1) & 1)
	//{
	//	if (!HookGAKS())
	//	{
	//		printf("Fuckin up hookin GAKS...\n");
	//	}
	//}

	//if (GetAsyncKeyState(VK_NUMPAD1) & 1)
	//{
	//	con.Write("Toggled Inf Ammo!");
	//	//toggle inf ammo
	//	if (!InfAmmo())
	//		con.Write("Failed to toggle infinite ammo!\n");
	//}

	if (GetAsyncKeyState(VK_NUMPAD2) & 1)
	{
		con.Write("Toggled God Mode!");
		//toggle inf ammo
		if (!GodMode())
			con.Write("Failed to toggle God Mode!\n");
	}
	return true;
}

void CInterface::Unload()
{
	if (infAmmo.bEnabled)
		InfAmmo();
	bRunning = false;
	con.~Console();
	HookManager.~CHookManager();
}

void CInterface::Init()
{

	dxHook.iSize = 7;
	dxHook.pDest = (uintptr_t)hEndScene;
	dxHook.regToUse = EAX;
	dxHook.pSource = (uintptr_t)d3dHook.GetDxDeviceFuncByIndex(0x2A);
	dxHook.sName = "dxEndScene";
	HookManager.CreateTrampHook(dxHook);
	oEndScene = (_EndScene)dxHook.pOriginalFunc;
	HookManager.ToggleHook(dxHook.sName);

	//oEndScene = (_EndScene)d3dHook.GetDxDeviceFuncByIndex(0x2A);
	//if (!HookManager.CreateVmtHook((uintptr_t)hEndScene, 0x2A, (void**)d3dHook.GetFunctionThisPtr((void*)oEndScene), "d3d"))
	//	con.Write("Failed to create VMT hook!\n");

	//HookManager.ToggleHook("d3d");
}

bool CInterface::HookGAKS()
{
	if (!HookManager.HookExists("gaks"))
	{
		HMODULE hUser32 = GetModuleHandleA("TraxinMemory.dll");
		if (!HookManager.CreateIATHook("gaks", "GetAsyncKeyState", hUser32, (uintptr_t)hGetAsyncKeyState))
		{
			con.Write("Failed to create GAKS IAT hook!\n");
			return false;
		}
		IATHook * pHook = (IATHook*)(HookManager.AccessHook("gaks"));
		oGetAsyncKeyState = (tGetAsyncKeyState)pHook->GetOldFuncAddress();
	}
	return HookManager.ToggleHook("gaks");
}

bool CInterface::InfAmmo()
{
	infAmmo.bEnabled = !infAmmo.bEnabled;
	if (infAmmo.bEnabled)
	{
		if (infAmmo.vOgBytes.empty())
		{
			if (!TraxMem::ReadBytes(infAmmo.pAddress, infAmmo.vOgBytes, infAmmo.iSize))
				return false;
		}

		TraxMem::WriteNops(infAmmo.pAddress, 2);
	}
	else
	{
		if (!TraxMem::WriteBytes(infAmmo.pAddress, infAmmo.vOgBytes))
			return false;
	}
	return true;
}

bool CInterface::GodMode()
{
	godMode.bEnabled = !godMode.bEnabled;

	if (godMode.bEnabled)
	{
		//mid function hook
		//allocate space for our hook, if needed.
		if (!godMode.pDest)
		 	godMode.pDest = (uintptr_t)VirtualAlloc(nullptr, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
		
		//setup data for the hook, if needed.
		if (!godMode.byteCave.bInitialized)
		{
			godMode.byteCave.bInitialized = true;
			godMode.byteCave.AddBytes(std::vector<byte>({ 0x3B, 0x35, 0x6C, 0xC5, 0x1D, 0x60, 0x0F, 0x85, 0x05, 0x00, 0x00, 0x00, 0xE9, 0x06, 0x00, 0x00, 0x00, 0xD9, 0x9E, 0xCC, 0x00, 0x00, 0x00, 0xE9 }));
			//godMode.vBytes = godMode.byteCave.GetBytes();

			uintptr_t relAddress = (godMode.pSource + (godMode.iSize - 5)) - (godMode.pDest + godMode.byteCave.GetCurrentSize());
			godMode.byteCave.AddRelAddress(relAddress);
			//write it to our hook, if needed.
			if (!godMode.bHookedOnce)
			{
				if (!godMode.byteCave.GetData(godMode.vBytes))
					return false;
				if (!TraxMem::WriteBytes(godMode.pDest, godMode.vBytes))
					return false;
				godMode.bHookedOnce = true;
			}
		}

		//create hook object, if needed... :D lol
		if(!HookManager.HookExists("godMode"))
			HookManager.CreateMidFunctionHook(godMode.pSource, godMode.pDest, godMode.iSize, "godMode");		
	}

	return HookManager.ToggleHook("godMode");
}

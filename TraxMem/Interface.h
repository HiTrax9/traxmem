#pragma once
#ifndef CINTERFACE_H
#define CINTERFACE_H

#include "TraxMem/TraxMem.h"
#include "TraxMem/HookManager.h"
#include "Console.h"
#include "GHooks.h"
#include "D3DHook.h"

extern Console * pCon;

struct SInfAmmo
{
	uintptr_t pAddress;
	uintptr_t offset = 0x100C6D;
	uint iSize = 2;
	bool bEnabled = false;
	std::vector<byte> vOgBytes;
};

struct SGodMode
{
	uintptr_t pSource;
	uintptr_t pSourceOffset = 0xF5DDC;
	uint iSize = 6;
	bool bUseJmp = false;
	bool bEnabled = false;
	uintptr_t pDest = 0;
	std::vector<byte> vBytes;
	bool bHookedOnce = false;
	ByteCave byteCave;
};

struct SGAKS
{
	uintptr_t pSource;
	uintptr_t pDest;
	uint iSize;
	EREGISTER reg;
	uintptr_t * trampBack;
};

class CInterface
{
public:
	CInterface();
	~CInterface();
	bool Update();
	void Unload();

public:
	bool bRunning = true;
private:
	void Init();
private:
	bool HookGAKS();
	bool InfAmmo();
	bool GodMode();

private:
	Console con;
private:
	CHookManager HookManager;
	uintptr_t pEntitiesMPdll;
	uintptr_t pEngineDll;
	SInfAmmo infAmmo;
	SGodMode godMode;
	SGAKS gaks;
	STrampHook dxHook;
	CD3DHook d3dHook;
	//SPattern DecHealthPattern;
};

#endif // !CINTERFACE_H
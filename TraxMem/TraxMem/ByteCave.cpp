#include "ByteCave.h"

ByteCave::ByteCave(std::vector<std::vector<byte>> byteArrays)
{
	this->byteArrays = byteArrays;
}

ByteCave::ByteCave(std::vector<std::vector<byte>> byteArrays, std::vector<uintptr_t> relAddresses)
{
	this->byteArrays = byteArrays;
	this->relAddresses = relAddresses;
}

std::string ByteCave::GetError()
{
	return sError;
}

uint ByteCave::GetCurrentSize()
{
	uint iAddyByteCount = relAddresses.size() * sizeof(uintptr_t);
	uint iByteCount = 0;
	for each(auto ba in byteArrays)
	{
		iByteCount += ba.size();
	}
	return iAddyByteCount + iByteCount;
}

void ByteCave::SetRelAddresses(std::vector<uintptr_t> RelAddresses)
{
	relAddresses = RelAddresses; 
}

void ByteCave::AddRelAddress(uintptr_t relAddress)
{
	relAddresses.push_back(relAddress);
}

void ByteCave::SetBytes(std::vector<std::vector<byte>> vByteArrays)
{
	byteArrays = vByteArrays;
}

void ByteCave::AddBytes(std::vector<byte> bytes)
{
	byteArrays.push_back(bytes);
}

bool ByteCave::GetData(std::vector<byte>& vBytes)
{
	uint baCount = byteArrays.size();
	uint raCount = relAddresses.size();

	if (!(baCount == (raCount + 1)))
	{
		if (!(baCount == 1 && raCount == 1))
		{
			sError = "ByteCave Error(GetBytes): Invalid parameters.";
			return false;
		}
	}

	if ((baCount == 1 && raCount == 1))
	{
		//Here we only have 1 array and one address, the retn address
		vBytes = byteArrays[0];
		std::vector<byte> vB = GetAddressBytes(relAddresses[0]);
		for each (byte b in vB)
			vBytes.push_back(b);
		return true;
	}
	else
	{
		uint x = 0;
		for each(auto ba in byteArrays)
		{
			//push each byte in this array into the return vector
			for each(byte b in ba)
				vBytes.push_back(b);

			//grab the byte from the address in the the addy vector
			std::vector<byte> vB = GetAddressBytes(relAddresses[x]);
			//push it's bytes into the return vector
			for each(byte b in vB)
				vBytes.push_back(b);
			x++;
			//continue on to the next array
		}
	}
	return true;
}

std::vector<byte> ByteCave::GetAddressBytes(const uintptr_t pAddress)
{
	std::vector<byte> vRet;

	char* pByte = (char*)&pAddress;

	for (int x = 0; x < sizeof(uintptr_t); x++)
	{
		vRet.push_back(pByte[x]);
	}

	return vRet;
}

std::vector<byte> ByteCave::GetBytes()
{
	if (byteArrays.empty())
		return std::vector<byte>();
	else
	{
		std::vector<byte> vRet;
		for each(auto v in byteArrays)
		{
			for each(byte b in v)
			{
				vRet.push_back(b);
			}
		}
		return vRet;
	}
}

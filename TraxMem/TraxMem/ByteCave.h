#pragma once
#ifndef BYTECAVE_H
#define BYTECAVE_H
#include "TraxIncludes.h"
#include "TraxMem.h"

class ByteCave
{
	std::string sError;
	std::vector<uintptr_t> relAddresses;
	std::vector<std::vector<byte>> byteArrays;

public:
	bool bInitialized = false;

public: // ctors
	ByteCave() = default;
	ByteCave(std::vector<std::vector<byte>> byteArrays);
	ByteCave(std::vector<std::vector<byte>> byteArrays, std::vector<uintptr_t> relAddresses);

public: // member funcs
	std::string GetError();
	uint GetCurrentSize();
	void SetRelAddresses(std::vector<uintptr_t> RelAddresses);
	void AddRelAddress(uintptr_t relAddress);
	void SetBytes(std::vector<std::vector<byte>> vByteArrays);
	void AddBytes(std::vector<byte> bytes);
	bool GetData(std::vector<byte> & vBytes);
	static std::vector<byte> GetAddressBytes(const uintptr_t pAddress);
	
	std::vector<byte> GetBytes();
};

#endif // !BYTECAVE_H
#include "HookManager.h"


CHookManager::~CHookManager()
{
	for each (auto p in HookList)
	{
		p.second->Unload();
		p.second->~Hook();
	}
}

bool CHookManager::HookExists(const char * szHookName) const 
{
	if (HookList.empty())
		return false;

	for each(auto h in HookList)
	{
		if (h.second->IsNamed(szHookName))
			return true;
	}
	return false;
}

bool CHookManager::HookExists(std::string sHookName) const
{
	return HookExists(sHookName.c_str());
}

bool CHookManager::CreateMidFunctionHook(uintptr_t pSource, uintptr_t pDest, uint iSize, std::string sName, bool bUseJMP)
{
	if (HookExists(sName))
		return false;

	MidFunctionHook * pHook = new MidFunctionHook(pSource, pDest, iSize, sName, bUseJMP);
	this->HookNames[sName] = iHookCount;
	this->HookList[iHookCount] = pHook;
	iHookCount++;
	return true;
}

bool CHookManager::CreateMidFunctionHook(uintptr_t pSource, uintptr_t pDest, uint iSize, const char * sName, bool bUseJMP)
{
	std::string name = sName;
	return CreateMidFunctionHook(pSource, pDest, iSize, name, bUseJMP);
}

bool CHookManager::CreateTrampHook(STrampHook & hookInfo)
{
	if (HookExists(hookInfo.sName))
		return false;

	TrampolineHook * pHook = new TrampolineHook(hookInfo);
	this->HookNames[hookInfo.sName] = iHookCount;
	this->HookList[iHookCount] = pHook;
	iHookCount++;
	return pHook->CreateTrampoline();
}

bool CHookManager::CreateVmtHook(std::string sName, uintptr_t pDest, uintptr_t pObject, int funcIndex, bool bIsObectPtr)
{
	if(HookExists(sName))
		return false;

	VMTHook * pHook = new VMTHook(sName, pDest, funcIndex, pObject, bIsObectPtr);
	this->HookNames[sName] = iHookCount;
	this->HookList[iHookCount] = pHook;
	iHookCount++;
	return true;
}

//bool CHookManager::CreateVmtHook(uintptr_t pDest, int funcIndex, void ** pVMT, std::string sName)
//{
//	if(HookExists(sName))
//		return false;
//
//	VMTHook * pHook = new VMTHook(pDest, funcIndex, pVMT, sName);
//	this->HookNames[sName] = iHookCount;
//	this->HookList[iHookCount] = pHook;
//	iHookCount++;
//	return true;
//}
//
//bool CHookManager::CreateVmtHook(std::string sName, uintptr_t pDest, uintptr_t pVmtEntryAddress)
//{
//	if (HookExists(sName))
//		return false;
//
//	VMTHook * pHook = new VMTHook(sName, pDest, pVmtEntryAddress);
//	this->HookNames[sName] = iHookCount;
//	this->HookList[iHookCount] = pHook;
//	iHookCount++;
//	return true;
//}

bool CHookManager::CreateIATHook(std::string sName, std::string sSymbol, std::string sModule, uintptr_t pDest)
{
	if(HookExists(sName))
		return false;

	IATHook * pHook = new IATHook(sName, sSymbol, sModule, pDest);
	return AddHook(sName, pHook);
}

bool CHookManager::CreateIATHook(std::string sName, std::string sSymbol, HMODULE hModule, uintptr_t pDest)
{
	if (HookExists(sName))
		return false;

	IATHook * pHook = new IATHook(sName, sSymbol, hModule, pDest);
	return AddHook(sName, pHook);
}

bool CHookManager::ToggleHook(const char * szHookName)
{
	if (HookExists(szHookName))
	{
		return HookList[HookNames[szHookName]]->Toggle();
	}
	return false;
}

bool CHookManager::ToggleHook(std::string sHookName)
{
	return ToggleHook(sHookName.c_str());
}

bool CHookManager::IsHooked(std::string sName) 
{
	if (HookList.empty())
		return false;

	return HookList[HookNames[sName]]->IsHooked();
}

Hook* CHookManager::AccessHook(std::string sName) const
{
	return HookList.at((const int)HookNames.at(sName));
}

bool CHookManager::AddHook(std::string sName, Hook * pHook)
{
	this->HookNames[sName] = iHookCount;
	this->HookList[iHookCount] = pHook;
	iHookCount++;
	return true;
}

#pragma once
#ifndef CHOOKMANAGER_H
#define CHOOKMANAGER_H
#include "TraxIncludes.h"
#include "TraxHooks.h"

class CHookManager
{
public:
	CHookManager() = default;
	~CHookManager();

	bool HookExists(const char* szHookName) const;
	bool HookExists(std::string sHookName) const;

	bool CreateMidFunctionHook(uintptr_t pSource, uintptr_t pDest, uint iSize, std::string sName, bool bUseJMP = true);
	bool CreateMidFunctionHook(uintptr_t pSource, uintptr_t pDest, uint iSize, const char* sName, bool bUseJMP = true);

	bool CreateTrampHook(STrampHook & hookInfo);

	//pretty much the only one working right now
	bool CreateVmtHook(std::string sName, uintptr_t pDest, uintptr_t pObject, int funcIndex, bool bIsObectPtr = false);
	//these aren't really working, 
	//i should really clean this whole vmt hooking thing because as easy 
	//as the whole process is, the code is way too messy.
	//bool CreateVmtHook(uintptr_t pDest, int funcIndex, void** pVMT, std::string sName);
	//bool CreateVmtHook(std::string sName, uintptr_t pDest, uintptr_t pVmtEntryAddress);

	bool CreateIATHook(std::string sName, std::string sSymbol, std::string sModule, uintptr_t pDest);
	bool CreateIATHook(std::string sName, std::string sSymbol, HMODULE hModule, uintptr_t pDest);
	bool ToggleHook(const char* szHookName);
	bool ToggleHook(std::string sHookName);
	bool IsHooked(std::string sName);
	Hook* AccessHook(std::string sName) const;
private:
	bool AddHook(std::string sName, Hook * pHook);

private:
	uint iHookCount = 0;
	std::map<std::string, int> HookNames;
	std::map<int, Hook*> HookList;

};

#endif // !CHOOKMANAGER_H

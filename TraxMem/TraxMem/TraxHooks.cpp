#include "TraxHooks.h"
#include "TraxMem.h"

MidFunctionHook::MidFunctionHook(uintptr_t pSrc, uintptr_t pDst, uint iSize, std::string sName, bool bUseJMP)
{
	bHooked = false;
	this->bUseJMP = bUseJMP;
	this->pSource = pSrc;
	this->pDest = pDst;
	this->iSize = iSize;
	this->sName = sName;
	this->HookType = EHookType::MIDFUNC;
}

bool MidFunctionHook::Toggle()
{
	bHooked = !bHooked;
	if (bHooked)
	{
		// Initialize the original bytes vector
		if (vOgBytes.size() == 0)
			if (!TraxMem::ReadBytes(pSource, vOgBytes, iSize))
				return false;

		// Write Hook
		return Hook();
	}
	else
	{
		bHooked = false;
		return TraxMem::WriteBytes(pSource, vOgBytes);
	}
}

bool MidFunctionHook::Hook()
{
	return TraxMem::Hook(this->pSource, this->pDest, this->iSize, this->bUseJMP);
}

TrampolineHook::TrampolineHook(STrampHook & hookInfo)
	:
	hookInfo(hookInfo)
{
	this->pSource = hookInfo.pSource;
	this->pDest = hookInfo.pDest;
	this->iSize = hookInfo.iSize;
	this->sName = hookInfo.sName;
	this->reg = hookInfo.regToUse;
}

bool TrampolineHook::Toggle()
{
	bHooked = !bHooked;
	if (bHooked)
	{
		return Hook();
	}
	else
	{
		return TraxMem::WriteBytes(pSource, vOgBytes);
	}
}

bool TrampolineHook::CreateTrampoline()
{
	//read the original bytes
	if (vOgBytes.empty())
	{
		if (!TraxMem::ReadBytes(pSource, vOgBytes, iSize))
			return false;
	}

	// allocate space for the tramp.
	uintptr_t pLoc = this->pSource - (0x1000 * 2);
	while (pTramp == 0)
	{
		pTramp = (uintptr_t)VirtualAlloc((void*)pLoc, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
		pLoc += 0x500;
	}
	TraxMem::WriteNops(pTramp, 0x100);

	//we're basically just moving the address of the dest to a register
	//then we're jumping to the hook(dest)
	// jmp [reg]
	*(byte*)(pTramp + 4) = 0xB8 | reg;
	if (!TraxMem::WriteValue<uintptr_t>(pTramp + 5, pDest))
		return false;

	if (!TraxMem::WriteBytes(pTramp + sizeof(uintptr_t) + 5, 
		std::vector<byte>(
	{ 0xFF, static_cast<byte>(0xE0|reg) ,	// jmp reg
	  0x31, static_cast<byte>(0xd8)})))	// xor reg, reg
		return false;


	//this is where the hook will "return" to before jumping back into the rest of the function.
	uintptr_t currLoc = pTramp + sizeof(uintptr_t) + 9;
	hookInfo.pOriginalFunc = currLoc;
	//write the original code
	if (!TraxMem::WriteBytes(currLoc, vOgBytes))
		return false;

	currLoc = currLoc + 1 + vOgBytes.size();
	//write the jump back
	if (!TraxMem::WriteBytes(currLoc, std::vector<byte>({ 0xE9 })))
		return false;

	uintptr_t relAddress = pSource - (currLoc - 2);

	currLoc++;
	if (!TraxMem::WriteValue<uintptr_t>(currLoc, relAddress))
		return false;

	return true;
}

bool TrampolineHook::Hook()
{
	return TraxMem::Hook(pSource, pTramp, iSize);
}

VMTHook::VMTHook(std::string sName, uintptr_t pDest, int funcIndex, uintptr_t pObject, bool bIsObjectPtr)
{
	this->sName = sName;
	this->pDest = pDest;
	this->funcIndex = funcIndex;
	if (!bIsObjectPtr)
		this->pObject = pObject;
	else
		this->pObject = TraxMem::ReadValue<uintptr_t>(pObject);
}
//
//VMTHook::VMTHook(uintptr_t pDest, uintptr_t pFunc, uintptr_t pObject, bool bIsObjectPtr)
//{
//	this->pFunc = pFunc;
//	//VMTHook(pDest, -1, pObject, bIsObjectPtr);
//}
//
//VMTHook::VMTHook(std::string sName, uintptr_t pDest, uintptr_t pFunc)
//{
//	this->sName = sName;
//	this->pDest = pDest;
//	this->pFunc = pFunc;
//}
//
//VMTHook::VMTHook(uintptr_t pDest, int funcIndex, void** pVMT, std::string sName)
//{
//	this->sName = sName;
//	this->pDest = pDest;
//	this->funcIndex = funcIndex;
//	this->pVMT = pVMT;
//	this->pSource = (uintptr_t)pVMT + (funcIndex * sizeof(uintptr_t));
//	pFunc = (uintptr_t)pVMT[funcIndex];
//}

void VMTHook::ReApply()
{
	if(TraxMem::ReadValue<uintptr_t>((uintptr_t)this->pVMT[funcIndex]) != this->pDest)
		Hook();
}

bool VMTHook::Toggle()
{
	bHooked = !bHooked;
	if (bHooked)
	{
		if (!bReady)
			if (!InitializeHook())
				return false;
		return Hook();
	}
	return TraxMem::WriteValue<uintptr_t>((uintptr_t)&pVMT[funcIndex], pFunc);
}

bool VMTHook::InitializeHook()
{
	if(!pVMT)
		pVMT = *(void***)pObject;
	if (funcIndex != -1)
	{
		if (!pVMT)
			return false;
		pFunc = (uintptr_t)pVMT[funcIndex];
		if (!pFunc)
			return false;

		pOldVmtEntry = pFunc;
		return true;
	}
	else if (pFunc != 0)
		pOldVmtEntry = TraxMem::ReadValue<uintptr_t>(pFunc);
	else
		return false;

	if (pOldVmtEntry == 0)
		return false;

	bReady = true;
	return true;
}

bool VMTHook::Hook()
{
	if (!TraxMem::WriteValue<uintptr_t>((uintptr_t)&pVMT[funcIndex], pDest))
		return false;

	//if (!TraxMem::WriteValue<uintptr_t>(pSource, pDest))
	//	return false;
	return true;
}

IATHook::IATHook(std::string sName, std::string sSymbol, std::string sModule, uintptr_t pDest)
{
	this->sName = sName;
	this->pDest = pDest;
	this->sSymbol = sSymbol;
	this->sModule = sModule;
	this->hModule = GetModuleHandleA(sModule.c_str());

	pSource = (uintptr_t)FindInIAT(sSymbol.c_str());
	pOldFunc = TraxMem::ReadValue<uintptr_t>(pSource);
}

IATHook::IATHook(std::string sName, std::string sSymbol, HMODULE hModule, uintptr_t pDest)
{
	this->sName = sName;
	this->pDest = pDest;
	this->sSymbol = sSymbol;
	this->hModule = hModule;

	pSource = (uintptr_t)FindInIAT(sSymbol.c_str());
	pOldFunc = TraxMem::ReadValue<uintptr_t>(pSource);
}

bool IATHook::Toggle()
{
	bHooked = !bHooked;
	if(bHooked)
	{
		return Hook();
	}
	return TraxMem::WriteValue<uintptr_t>(pSource, this->pOldFunc);
}

uintptr_t IATHook::GetOldFuncAddress()
{
	return pOldFunc;
}

void ** IATHook::FindInIAT(const char * sFunction)
{
	if (!hModule)
		hModule = GetModuleHandle(0);

	PIMAGE_DOS_HEADER img_dos_headers = (PIMAGE_DOS_HEADER)hModule;
	PIMAGE_NT_HEADERS img_nt_headers = (PIMAGE_NT_HEADERS)((byte*)img_dos_headers + img_dos_headers->e_lfanew);
	PIMAGE_IMPORT_DESCRIPTOR img_import_desc = (PIMAGE_IMPORT_DESCRIPTOR)((byte*)img_dos_headers + img_nt_headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
	if (img_dos_headers->e_magic != IMAGE_DOS_SIGNATURE)
		printf("ERROR: e_magic is not a valid DOS signature\n");

	for (IMAGE_IMPORT_DESCRIPTOR *iid = img_import_desc; iid->Name != 0; iid++)
	{
		for (int func_idx = 0; *(func_idx + (void**)(iid->FirstThunk + (size_t)hModule)) != nullptr; func_idx++)
		{
			char* mod_func_name = (char*)(*(func_idx + (size_t*)(iid->OriginalFirstThunk + (size_t)hModule)) + (size_t)hModule + 2);
			const intptr_t nmod_func_name = (intptr_t)mod_func_name;
			if (nmod_func_name >= 0)
			{
				if (!::strcmp(sFunction, mod_func_name))
					return func_idx + (void**)(iid->FirstThunk + (size_t)hModule);
			}
		}
	}

	return 0;
}

bool IATHook::Hook()
{
	if(!TraxMem::WriteValue<uintptr_t>(this->pSource, pDest))
		return false;
	return true;
}

VEHook::VEHook(uintptr_t pSource, uintptr_t pDest)
{
	this->pSource = pSource;
	this->pDest = pDest;
}

#pragma once
#ifndef TRAXHOOKS_H
#define TRAXHOOKS_H
#include "TraxIncludes.h"

enum EREGISTER : byte
{
	EAX,
	ECX,
	EDX,
	EBX,
	ESP,
	EBP,
	ESI,
	EDI
};

struct SHook
{
	uintptr_t pSource;
	uintptr_t pDest;
	uint iSize;
	std::string sName;
};

struct STrampHook : SHook
{
	uintptr_t pOriginalFunc;
	EREGISTER regToUse;
};

class Hook
{
protected:
	bool bHooked = false;
	std::string sName;

public:
	enum EHookType
	{
		MIDFUNC,
		VMT,
		IAT,
		TRAMP
	};
	bool IsHooked() { return bHooked; }
	bool IsNamed(const char* szHookName) { return (!lstrcmpA(szHookName, sName.c_str())) ? true : false; }
	void Unload()
	{
		if (bHooked)
			Toggle();
	}
	virtual bool Toggle() = 0;

public:
	//This is the original location of our hook.
	uintptr_t pSource;

	//Size of bytes the original instruction/pointer takes.
	uint iSize;

	//Destination of our hook.
	uintptr_t pDest;

	//Vector of the original bytes. Be it for a midfunction hook, or the original address of a vmt/iat entry.
	std::vector<byte> vOgBytes;

	//whatever type of hook it may be.
	EHookType HookType;

};

class MidFunctionHook : public Hook
{
public:
	// bUseJMP = Choose between using either a JMP or a CALL instruction.
	MidFunctionHook(uintptr_t pSrc, uintptr_t pDst, uint iSize, std::string sName, bool bUseJMP = true);
	bool Toggle();
private:
	bool Hook();

private: // It's one and only member... :(
	bool bUseJMP;
};

class TrampolineHook : public Hook
{
public:
	TrampolineHook(STrampHook & hookInfo);
	bool Toggle();
	bool CreateTrampoline();

private:
	bool Hook();

private:
	STrampHook & hookInfo;
	EREGISTER reg;
	uintptr_t pTramp = 0;
	uintptr_t pTrampBack = 0;
};

class VMTHook : public Hook
{
public://ctors	
	   // if bIsObjectPtr is true, the address will be dereferenced
	   // and the value will be used as the this pointer.
	VMTHook(std::string sName, uintptr_t pDest, int funcIndex, uintptr_t pObject, bool bIsObjectPtr = false);
	//// pFunc is the address of the target function in the vTable.
	//VMTHook(uintptr_t pDest, uintptr_t pFunc, uintptr_t pObject, bool bIsObjectPtr = false);
	//
	//VMTHook(std::string sName, uintptr_t pDest, uintptr_t pFunc);
	//
	//VMTHook(uintptr_t pDest, int funcIndex, void** pVMT, std::string sName);
	void ReApply();
public:
	bool Toggle();

private:
	bool InitializeHook();
	bool Hook();

private:
	uintptr_t pObject = 0;
	uintptr_t pOldVmtEntry = 0;
	uintptr_t pFunc = 0;
	int funcIndex = -1;
	bool bReady = false;
	void ** pVMT = nullptr;
};

class IATHook : public Hook
{
public:
	IATHook(std::string sName, std::string sSymbol, std::string sModule, uintptr_t pDest);
	IATHook(std::string sName, std::string sSymbol, HMODULE hModule, uintptr_t pDest);
	bool Toggle();
	uintptr_t GetOldFuncAddress();
private:
	void ** FindInIAT(const char* sFunction);
	bool Hook();

private:
	std::string sSymbol;
	std::string sModule;
	HMODULE hModule;
	uintptr_t pOldFunc = 0;
};

class VEHook : public Hook
{
public:
	VEHook(uintptr_t pSource, uintptr_t pDest);

private:

};
#endif // !TRAXHOOKS_H

#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#include <algorithm>
#include <vector>
#include <map>
#include <string>

typedef UINT uint;
typedef unsigned char byte;
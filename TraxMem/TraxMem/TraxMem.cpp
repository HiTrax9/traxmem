#include "TraxMem.h"

std::string TraxMem::sError = "No error...";

uintptr_t TraxMem::EvaluatePointer(uintptr_t pBase, uint * pOffsets, uint iOffsetCount)
{
	uintptr_t pResolved = *(uintptr_t*)pBase;

	for (uint i = 0; i < iOffsetCount - 1; i++)
	{ 
		uintptr_t buff = *(uintptr_t*)(pResolved + pOffsets[i]);
		if ((void*)buff != nullptr && buff > (uintptr_t)GetModuleHandle(0))
			pResolved = buff;
		else
		{
			SetError("Failed to evaluate pointer.");
			return uintptr_t(0);
		}
	}

	return (pResolved + pOffsets[iOffsetCount - 1]);
}

uintptr_t TraxMem::EvaluatePointer(uintptr_t pBase, std::vector<uint> vOffsets)
{
	uintptr_t pResolved = *(uintptr_t*)pBase;
	uint vSize = vOffsets.size();

	for (uint i = 0; i < vSize - 1; i++)
	{
		uintptr_t buff = *(uintptr_t*)(pResolved + vOffsets[i]);
		if ((void*)buff != nullptr)
			pResolved = buff;
		else
		{
			SetError("Failed to evaluate pointer.");
			return uintptr_t(0);
		}
	}

	return (pResolved + vOffsets[vSize - 1]);
}

bool TraxMem::WriteNops(uintptr_t pAddress, uint iByteCount)
{
	DWORD dwOldProt;

	if (!VirtualProtect((void*)pAddress, iByteCount, PAGE_EXECUTE_READWRITE, &dwOldProt))
	{
		SetError("Failed to write nops:\n", true);
		return false;
	}

	memset((void*)pAddress, 0x90, iByteCount);
	VirtualProtect((void*)pAddress, iByteCount, dwOldProt, nullptr);
	return true;
}

bool TraxMem::WriteBytes(uintptr_t pAddress, byte * pBytes, uint iByteCount)
{
	DWORD dwOldProt;

	if (!VirtualProtect((void*)pAddress, iByteCount, PAGE_EXECUTE_READWRITE, &dwOldProt))
	{		
		SetError("Failed to write bytes:\n", true);
		return false;
	}

	memcpy((void*)pAddress, pBytes, iByteCount);
	VirtualProtect((void*)pAddress, iByteCount, dwOldProt, nullptr);
	return true;
}

bool TraxMem::WriteBytes(uintptr_t pAddress, std::vector<byte> vBytes)
{
	DWORD dwOldProt;
	uint vSize = vBytes.size();

	if (!VirtualProtect((void*)pAddress, vSize, PAGE_EXECUTE_READWRITE, &dwOldProt))
	{
		SetError("Failed to write bytes:\n", true);
		return false;
	}
	
	byte * ba = new byte[vSize];
	for (uint x = 0; x < vSize; x++)
		ba[x] = vBytes[x];
		
	memcpy((void*)pAddress, ba, vSize);
	
	VirtualProtect((void*)pAddress, vSize, dwOldProt, nullptr);
	delete[] ba;

	return true;	
}

bool TraxMem::ReadBytes(uintptr_t pAddress, byte * pBytes, uint iSize)
{
	DWORD dwOld;
	if (!VirtualProtect((void*)pAddress, iSize, PAGE_EXECUTE_READWRITE, &dwOld))
	{		
		SetError("Failed to read bytes:\n", true);
		return false;
	}

	byte * ba = new byte[iSize];
	
	for (uint x = 0; x < iSize; x++)
		ba[x] = *(byte*)(pAddress + x);

	memcpy(pBytes, ba, iSize);
	delete[] ba;
	return true;
}

bool TraxMem::ReadBytes(uintptr_t pAddress, std::vector<byte>& vBytes, uint iSize)
{
	DWORD dwOld;
	if (!VirtualProtect((void*)pAddress, iSize, PAGE_EXECUTE_READWRITE, &dwOld))
	{
		SetError("Failed to read bytes:\n", true);
		return false;
	}

	for (uint x = 0; x < iSize; x++)
		vBytes.push_back(*(byte*)(pAddress + x));

	return true;
}

bool TraxMem::PatternScanMod(SPattern pattern, std::string sModule, uintptr_t & pResult)
{
	return PatternScanMod(pattern, (HMODULE)GetModuleHandleA(sModule.c_str()), pResult);
}

bool TraxMem::PatternScanMod(SPattern pattern, HMODULE hMod, uintptr_t & pResult)
{
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	
	MODULEINFO modInfo{ 0 };
	GetModuleInformation(GetCurrentProcess(), hMod, &modInfo, sizeof(MODULEINFO));
	uintptr_t iSize = modInfo.SizeOfImage;

	MEMORY_BASIC_INFORMATION mbi{ 0 };
	uintptr_t pLoc = (uintptr_t)modInfo.lpBaseOfDll;
	while (VirtualQuery((void*)(pLoc + si.dwPageSize), &mbi, sizeof(mbi)))
	{		
		if ((mbi.State != MEM_FREE) && (mbi.Protect >= 0x010 && mbi.Protect < 0x80))
		{
			/*if (PatternScan(pattern, pLoc, pLoc + si.dwPageSize, pResult))
				return true;*/
			if (Scan(pattern, pLoc, pLoc + si.dwPageSize, pResult))
				return true;
		}
		else
		{
			pLoc += si.dwPageSize;
			continue;
		}
		pLoc += si.dwPageSize;
		if ( pLoc >= ((uintptr_t)(modInfo.lpBaseOfDll) + modInfo.SizeOfImage))
			break;
	}
	return false;
}

bool TraxMem::PatternScanProcess(SPattern pattern, uintptr_t & pResult)
{
	HANDLE hMod;
	hMod = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());
	MODULEENTRY32 me32{ 0 };
	me32.dwSize = sizeof(MODULEENTRY32);

	if (Module32First(hMod, &me32))
	{		
		if (!PatternScanMod(pattern, me32.hModule, pResult))
		{
			while (Module32Next(hMod, &me32))
			{
				if (PatternScanMod(pattern, me32.hModule, pResult))
					return true;
			}
		}
		else
			return true;
	}
	return false;
}

bool TraxMem::Scan(SPattern pattern, uintptr_t pStart, uintptr_t pEnd, uintptr_t & pResult)
{
	DWORD dwOld;
	if (VirtualProtect((void*)pStart, pEnd - pStart, PAGE_EXECUTE_READWRITE, &dwOld))
	{
		byte* result = std::search((byte*)pStart, (byte*)pEnd, pattern.vPatternBytes.begin(), pattern.vPatternBytes.end(),
			[](byte b1, byte b2) { return b1 == b2 || b2 == '\x0'; });

		VirtualProtect((void*)pStart, pEnd - pStart, dwOld, nullptr);
		if ((uintptr_t)result < pEnd)
		{
			pResult = (uintptr_t)result;
			return true;
		}
	}
	pResult = 0;
	return false;
}

bool TraxMem::PatternScan(SPattern pattern, uintptr_t pStart, uintptr_t pEnd, uintptr_t & pResult)
{
	DWORD dwOld;
	uint patternCounter = 0;
	if (VirtualProtect((void*)pStart, pEnd - pStart, PAGE_EXECUTE_READWRITE, &dwOld))
	{
		for (uintptr_t x = pStart; x < pEnd; x++)
		{
			if (*(byte*)x == pattern.vPatternBytes[patternCounter] || pattern.mask[patternCounter] == '?')
			{
				patternCounter++;
				if (patternCounter == pattern.iPatternLen)
				{
					pResult = x - patternCounter + 1;
					return true;
				}
			}
			else
			{
				patternCounter = 0;
			}
		}
		VirtualProtect((void*)pStart, pEnd - pStart, dwOld, nullptr);
	}
	return false;
}

bool TraxMem::Hook(uintptr_t pSource, uintptr_t pDest, uint iSize, bool bUseJMP)
{
	if (iSize < 5)
		return false;

	DWORD dwOld;
	if (!VirtualProtect((void*)pSource, iSize, PAGE_EXECUTE_READWRITE, &dwOld))
		return false;

	memset((void*)pSource, 0x90, iSize);

	uintptr_t relAddress = pDest - pSource - 5;
	*(byte*)pSource = bUseJMP ? 0xE9 : 0xE8;
	*(uintptr_t*)(pSource + 1) = relAddress;

	VirtualProtect((void*)pSource, iSize, dwOld, nullptr);
	return true;
}

std::string TraxMem::GetError()
{
	return sError;
}

void TraxMem::SetError(std::string szError, bool bGetLastError)
{
	if (!bGetLastError)
		sError = szError;
	else
		sError = szError + GetLastErrorString();
}



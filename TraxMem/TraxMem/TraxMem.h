#pragma once
#ifndef TRAXMEM_H
#define TRAXMEM_H
#include "TraxIncludes.h"
#include "Util.h"

struct SPattern
{
public: // ctors
	SPattern() = default;
	//\x90\x90\x00\x00\x90\x90 xx??xx
	SPattern(char * sPattern, std::string mask)
	{		
		this->mask = mask;
		this->iPatternLen = mask.length();
		
		for (uint x = 0; x < iPatternLen; x++)
		{
			vPatternBytes.push_back(sPattern[x]);
		}
		if (vPatternBytes.size() != iPatternLen)
			SetError("Pattern and mask don't seem to be the same length!");
	}
	//\x90\x90\x00\x00\x90\x90 xx??xx
	SPattern(std::vector<byte> vPattern, std::string mask)
	{
		this->vPatternBytes = vPattern;
		this->mask = mask;
		this->iPatternLen = vPatternBytes.size();
		if (iPatternLen != mask.length())
			SetError("Pattern and mask don't seem to be the same length!");
	}
	// 90 ? 90 90 90 ? ? ? ? 90 90 
	SPattern(std::string sPattern)
	{
		if (IsValidHexChar(sPattern[0]))
		{
			char halfByte = 0;
			char cByte[3]{ 0 };

			for (uint x = 0; x < sPattern.length() - 1; x++)
			{
				if (IsValidHexChar(sPattern[x]) || sPattern[x] == '?')
				{
					if (sPattern[x] != '?')
					{
						cByte[halfByte] = sPattern[x];
						if (halfByte == 1)
						{
							halfByte = -1;
							char wholeByte[3]{ 0 };
							hex2bin(cByte, wholeByte);
							this->vPatternBytes.push_back(wholeByte[0]);
							this->mask += 'x';
						}
						halfByte++;
					}
					else
					{
						this->vPatternBytes.push_back(0);
						this->mask += '?';
					}
				}
			}
			this->iPatternLen = vPatternBytes.size();
		}
		else
			SetError("Invalid pattern");
	}
private:
	void SetError(const char* szError)
	{
		bError = true;
		sError = szError;
	}
public: // meme-bers
	uint iPatternLen = 0;
	std::string sPattern;
	std::string mask;
	std::vector<byte> vPatternBytes;
	std::string sError = "No error...";
	bool bError = false;
};

class TraxMem
{
public: 
	static uintptr_t EvaluatePointer(uintptr_t pBase, uint * pOffsets, uint iOffsetCount);
	static uintptr_t EvaluatePointer(uintptr_t pBase, std::vector<uint>vOffsets);
	
	static bool WriteNops(uintptr_t pAddress, uint iByteCount);
	
	static bool WriteBytes(uintptr_t pAddress, byte * pBytes, uint iByteCount);
	static bool WriteBytes(uintptr_t pAddress, std::vector<byte> vBytes);

	static bool ReadBytes(uintptr_t pAddress, byte* pBytes, uint iSize);
	static bool ReadBytes(uintptr_t pAddress, std::vector<byte> & vBytes, uint iSize);

public: // templated readers/writers

	template <class T>
	static T ReadValue(uintptr_t pAddress);

	template <class T>
	static T ReadValue(uintptr_t pBase, uint * pOffsets, uint iOffsetCount);

	template <class T>
	static T ReadValue(uintptr_t pBase, std::vector<uint>vOffsets);

	template <class T>
	static bool WriteValue(uintptr_t pAddress, T data);

	template <class T>
	static bool WriteValue(uintptr_t pBase, uint * pOffsets, uint iNumOffsets, T data);

	template <class T>
	static bool WriteValue(uintptr_t pBase, std::vector<uint> vOffsets, T data);	

public: // pattern scanning
	static bool PatternScanMod(SPattern pattern, std::string sModule, uintptr_t & pResult);
	static bool PatternScanMod(SPattern pattern, HMODULE hMod, uintptr_t & pResult);

	// not the quickest, but it works.
	static bool PatternScanProcess(SPattern pattern, uintptr_t & pResult);

private:
	static bool Scan(SPattern pattern, uintptr_t pStart, uintptr_t pEnd, uintptr_t & pResult);
	static bool PatternScan(SPattern pattern, uintptr_t pStart, uintptr_t pEnd, uintptr_t & pResult);

public: // simple hooking function
	static bool Hook(uintptr_t pSource, uintptr_t pDest, uint iSize, bool bUseJMP = true);

public: // error stuff
	static std::string GetError();
private:
	static void SetError(std::string szError, bool bGetLastError = false);

	// data members
public:

private:
	static std::string sError;
};

template<class T>
inline T TraxMem::ReadValue(uintptr_t pAddress)
{
	return *(T*)pAddress;
}

template<class T>
inline T TraxMem::ReadValue(uintptr_t pBase, uint * pOffsets, uint iOffsetCount)
{
	return *(T*)TraxMem::EvaluatePointer(pBase, pOffsets, iOffsetCount);
}

template<class T>
inline T TraxMem::ReadValue(uintptr_t pBase, std::vector<uint> vOffsets)
{
	uint size = vOffsets.size();
	uint * ba = new uint[size];
	for (uint x = 0; x < size; x++)
		ba[x] = vOffsets[x];

	T t = ReadValue<T>(pBase, ba, size);
	delete[] ba;
	return t;
}

template<class T>
inline bool TraxMem::WriteValue(uintptr_t pAddress, T data)
{
	DWORD dwOld;
	if (VirtualProtect((void*)pAddress, sizeof(T), PAGE_EXECUTE_READWRITE, &dwOld))
	{
		*(T*)pAddress = data;
		return true;
	}
	else
		return false;
}

template<class T>
inline bool TraxMem::WriteValue(uintptr_t pBase, uint * pOffsets, uint iNumOffsets, T data)
{
	DWORD dwOld;
	void* pAddress = (void*)EvaluatePointer(pBase, pOffsets, iNumOffsets);
	
	if (pAddress && pAddress > (void*)GetModuleHandle(0))
	{
		if (!VirtualProtect(pAddress, sizeof(T), PAGE_EXECUTE_READWRITE, &dwOld))
			return false;

		*(T*)pAddress = data;
		VirtualProtect(pAddress, sizeof(T), dwOld, nullptr);
	}
	else
		return false;
}

template<class T>
inline bool TraxMem::WriteValue(uintptr_t pBase, std::vector<uint> vOffsets, T data)
{

	uint size = vOffsets.size();
	uint * ba = new uint[size];
	for (uint x = 0; x < size; x++)
		ba[x] = vOffsets[x];

	bool bStatus = WriteValue<T>(pBase, ba, size, data);
	delete[] ba;
	return bStatus;
}
#include "ByteCave.h"
#endif // !TRAXMEM_H
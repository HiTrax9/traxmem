#pragma once
#ifndef UTIL_H
#define UTIL_H
#include "TraxIncludes.h"

//misc functions used by the memory class and whatever else. 
//They're in the global space.

int char2int(char input);
void hex2bin(const char* src, char* target);
bool IsValidHexChar(char c);

//calls GetLastError
std::string GetLastErrorString();



#endif // !UTIL_H
#include <Windows.h>
#include "TraxMem\TraxMem.h"
#include "Interface.h"

DWORD WINAPI MainThread(LPVOID param)
{
	CInterface Interface;
	while (Interface.Update());
	FreeLibraryAndExitThread((HMODULE)param, 0);
	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hModule, DWORD dwReason, LPVOID lpReserved)
{
	switch (dwReason)
	{
		case DLL_PROCESS_ATTACH:
			CreateThread(0, 0, MainThread, hModule, 0, 0);
			break;
		default:
			break;
	}
	return TRUE;
}